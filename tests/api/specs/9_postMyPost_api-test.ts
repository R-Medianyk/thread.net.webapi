import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { PostsController } from "../lib/controllers/post.controller";
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();
const posts = new PostsController();

xdescribe("Token usage for post", () => {
    let accessToken: string;

    before(`Login and get the token fot post`, async () => {
        let response = await auth.login("r.medianyk@gmail.com", "monkey-momo");

        accessToken = response.body.token.accessToken.token;
        // console.log(accessToken);
    });

    it(`should return status 200 after post`, async () => {
        let postData: object = {
            autorId: 1331,
            previewImage: "string",
            body: "Vi svegliate al parco senza vestiti ogni mattina",
        };
        let response = await posts.postMyPost(postData, accessToken);
        checkStatusCode(response, 200);
    });
});

