import { expect } from "chai";
import { checkResponseTime, checkStatusCode } from "../../helpers/functionsForChecking.helper";
import { UsersController } from "../lib/controllers/users.controller";

const users = new UsersController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Users controller`, () => {
    let userId: number;
 
    it(`should return 200 status code and all users when getting the user collection`, async () => {
        let response = await users.getAllUsers();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        
    });

    it(`should return 404 error when getting user details with invalid id`, async () => {
        let invalidUserId = 123133

        let response = await users.getUserById(invalidUserId);

        checkStatusCode(response, 404);
        checkResponseTime(response, 3000);
    });

    it(`should return 400 error when getting user details with invalid id type`, async () => {
        let invalidUserId = '2183821367281387213781263'

        let response = await users.getUserById(invalidUserId);

        checkStatusCode(response, 400);
        checkResponseTime(response, 3000);
    });

    it(`should return user details when getting user details with valid id`, async () => {
        let response = await users.getAllUsers();
        let firstUserId: number = response.body[0].id;
        
        response = await users.getUserById(firstUserId);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000); 

        // console.log(response.body);
    });
    
    

    it(`should return status 401 when getting user token`, async () => {
        let invalidaccessToken = `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJSb3N0eXNsYXYiLCJlbWFpbCI6InIubWVkaWFueWtAZ21haWwuY29tIiwianRpIjoiNDI4NzI4NGYtYzAzYi00YTZjLWJmODAtOTNhN2IyYzY3NGFmIiwiaWF0IjoxNjc3NTEzNTU5LCJpZCI6IjEzMzEiLCJuYmYiOjE2Nzc1MTM1NTgsImV4cCI6MTY3NzUyMDc1OCwiaXNzIjoiVGhyZWFkIC5ORVQgV2ViQVBJIiwiYXVkIjoiaHR0cHM6Ly9sb2NhbGhvc3Q6NDQzNDQifQ.MG5VEevSs4mCBAm4LG3r42-1oJ8iR5d_f7RBi9jd_xk`
        let response = await users.getUserByToken(invalidaccessToken);

        checkStatusCode(response, 401);
    })

});
