import { expect } from "chai";
import { checkResponseTime, checkStatusCode } from "../../helpers/functionsForChecking.helper";
import { PostsController } from "../lib/controllers/post.controller";

const users = new PostsController();
const schemas = require('./data/schemas_testDataFotPost.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Posts controller`, () => {
    let postId: number;

    it(`should return 200 status code and all users when getting the user collection`, async () => {
        let response = await users.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
        expect(response.body).to.be.jsonSchema(schemas.schema_allPosts); 
        
        postId = response.body[1].id;
    });

});