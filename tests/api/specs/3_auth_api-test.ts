import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";

const users = new UsersController();
const auth = new AuthController();

xdescribe("Token usage", () => {
    let accessToken: string;

    before(`Login and get the token`, async () => {
        let response = await auth.login("r.medianyk@gmail.com", "monkey-momo");

        accessToken = response.body.token.accessToken.token;
        // console.log(accessToken);
    });

    it(`Usage is here`, async () => {
        let userData: object = {
            id: 1331,
            avatar: "string",
            email: "r.medianyk@gmail.com",
            userName: "Rostyslav",
        };

        let response = await users.updateUser(userData, accessToken);
        expect(response.statusCode, `Status Code should be 204`).to.be.equal(204);
    });
});
