import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();

xdescribe("Update user | with hooks", () => {
    let accessToken: string;
    let userDataBeforeUpdate, userDataToUpdate;

    before(`Login and get the token`, async () => {
        let response = await auth.login("r.medianyk@gmail.com", "monkey-momo");
        checkStatusCode(response, 200);

        accessToken = response.body.token.accessToken.token;
    });

    it(`should return correct details of the currect user by token`, async () => {
        let response = await users.getUserByToken(accessToken);
        checkStatusCode(response, 200);
        userDataBeforeUpdate = response.body;
    });
});
