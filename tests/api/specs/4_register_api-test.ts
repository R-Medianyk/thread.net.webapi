import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { RegisterController } from "../lib/controllers/register.controller";

const users = new UsersController();
const register = new RegisterController();

xdescribe("Entering data", () => {
    let userId: number;
    let avatar: string;
    let email: string;
    let userName: string;
    let password: string;

    it(`Usage is here`, async () => {
        let userData: object = {
            id: 1331,
            avatar: "string",
            email: "r.medianyk@gmail.com",
            userName: "Rostyslav",
            password: "monkey-momo"
        };

        let response = await users.getUserById(userData)
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    });
});

