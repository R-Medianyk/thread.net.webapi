import { ApiRequest } from "../request";

export class RegisterController {
    async register(idValue: number, avatarValue: string, emailValue: string, nameValue: string, passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Register`)
            .body({
                id: idValue,
                avatar: avatarValue,
                email: emailValue,
                userName: nameValue,
                password: passwordValue,
            })
            .send();
        return response;
    }
}
